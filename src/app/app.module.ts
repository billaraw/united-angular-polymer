import { NgModule, CUSTOM_ELEMENTS_SCHEMA }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent }  from './app.component';
import { AppRoutingModule }        from './app.routing';

import { UaFlights } from './ua-flights.component';
import { UaHome } from './ua-home.component';

@NgModule({
  imports:      [ BrowserModule, AppRoutingModule ],
  declarations: [ AppComponent, UaFlights, UaHome ],
  bootstrap:    [ AppComponent ],
  schemas: [
      CUSTOM_ELEMENTS_SCHEMA
  ],
})
export class AppModule { }

import { NgModule }             from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UaFlights } from './ua-flights.component';
import { UaHome } from './ua-home.component';

const appRoutes: Routes = [
  {
    path: 'flights',
    component: UaFlights,
    data: {
      data: {
        title: 'United Flights'
      }
    }
  },
  {
    path: '',
    component: UaHome,
    data: {
      title: 'United Homepage'
    }
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }

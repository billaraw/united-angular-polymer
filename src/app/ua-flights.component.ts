import { Component } from '@angular/core';

class Flight {
  number: number;
  destination: string;

  constructor(number: number, destination: string){
      this.number = number;
      this.destination = destination;
  }
}

@Component({
  selector: 'ua-flights',
  templateUrl: './ua-flights.component.html',
})
export class UaFlights {
  flights: Flight[] = [];
  constructor() { this.getFlights(); }
  //Ajax call here
  getFlights() {
    this.flights = [new Flight(345, "Florida"), new Flight(643, "Chicago"), new Flight(7765, "New York"), new Flight(345, "Athens")]
  }
}
